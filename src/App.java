import InvoiceItem_Java.InvoiceItem;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        
        InvoiceItem invoice1 = new InvoiceItem("001", "house renting payment", 2, 200500);
    
        System.out.println("Invoice_1: ");
        System.out.println(invoice1);
        System.out.println("             Total amount: " + invoice1.getTotal());
    }
}
