
package InvoiceItem_Java;

import java.text.DecimalFormat;

public class InvoiceItem {
    
    private String id;
    private String desc;
    private int qty;
    private double unitPrice;
    
    public InvoiceItem(String id, String desc, int qty, double unitPrice) {
        this.id = id;
        this.desc = desc;
        this.qty = qty;
        this.unitPrice = unitPrice;
    }

    public String getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }

    public int getQty() {
        return qty;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getTotal() {
        double total = qty * unitPrice;
        
        DecimalFormat converted = new DecimalFormat("###,###.##");
        String formated = converted.format(total);
        return formated;
        
    }

    public String toString() {
        return "InvoiceItem: [id=" + id + ", desc= " + desc + ", qty= " + qty + ", unitPrice= " + unitPrice +  "]";
    }
}
